# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
#ZSH_THEME="terminalparty"
ZSH_THEME="zhann"

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git)

source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# Preferred editor
export EDITOR='nvim'

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
TERM=xterm-256color
alias sudo='sudo '
alias toss='rmtrash'
alias tossdir='rmdirtrash'
alias i3cfg='nvim ~/.config/i3/config'
alias pyy='cd ~/Documents/Python\ Projects/ && ls'
alias ccd='cd ~/Documents/School/C#/InleidingProgrammeren/InleidingProgrammeren'
alias csrun='~/Scripts/csrun.sh'
alias up='yay'
alias s='sudo'
alias it='yay -S'
alias un='sudo pacman -Rs'
alias sr="yay -s"
alias mu="ncmpcpp"
alias music="ncmpcpp"
alias bk="cd .."
alias back="cd .."
alias count="ls -1 | wc -l"
alias sabnzbd="python /opt/sabnzbd/SABnzbd.py"
alias cp="rsync --info=progress2"

# Credit to Luke Smith (lukesmithxyz) for some of these.
alias progs="(pacman -Qet && pacman -Qm) | sort -u"
alias v="nvim"
alias vim="nvim"
alias ka="killall"
alias sv="sudo nvim"
alias r="ranger"
alias ls='ls -hN --color=auto --group-directories-first'
alias g="git"
alias gitup="git push origin master"
alias mkdir="mkdir -pv"
alias crep="grep --color=always"
alias sdn="shutdown now"
alias yt="youtube-dl -ic"
alias yta="youtube-dl -xic"
alias starwars="telnet towel.blinkenlights.nl"
alias nf="clear && neofetch"
alias newnet="sudo systemctl restart NetworkManager"
alias tr="transmission-remote"
alias bw="wal -i -g ~/Pictures/wallsnieuw"
alias cl="clear"

#Music
alias pause="mpc toggle"
alias next="mpc next"
alias prev="mpc prev"
alias trupause="mpc pause"
alias beg="mpc seek 0%"
alias lilbak="mpc seek -10"
alias lilfor="mpc seek +10"
alias bigbak="mpc seek -120"
alias bigfor="mpc seek +120"

#Directory Shortcuts:
alias h="cd ~ && ls -a"
alias d="cd ~/Downloads && ls -a"
alias D="cd ~/Documents && ls -a"
alias p="cd ~/Pictures && ls -a"
alias v="cd ~/Videos && ls -a"
alias m="cd ~/Music && ls -a"
alias r="cd / && ls -a"
alias cf="cd ~/.config && ls -a"
alias S="cd ~/Sync && ls -a"
alias W="cd ~/Documents/Websites && ls -a"

alias cfz="nvim ~/.zshrc"
alias cfr="nvim ~/.config/ranger/rc.conf.base"
alias cfi="nvim ~/.config/i3/config"
alias cfp="nvim ~/.config/polybar/config"
alias cfx="nvim ~/.Xresources"

# Activate zsh-syntax-highlighting
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
